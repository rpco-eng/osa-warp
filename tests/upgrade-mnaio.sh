#!/usr/bin/env bash

# Copyright 2017, Rackspace US, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

## Shell Opts ----------------------------------------------------------------

set -evu

echo "Upgrading a Multi Node AIO (MNAIO)"
echo "+-------------------- MNAIO ENV VARS --------------------+"
env
echo "+-------------------- MNAIO ENV VARS --------------------+"

# ssh command used to execute tests on infra1
export MNAIO_SSH="ssh -ttt -oStrictHostKeyChecking=no root@infra1"

export RE_JOB_UPGRADE_TO=${RE_JOB_UPGRADE_TO:-'queens'}

# Run upgrades
${MNAIO_SSH} "source /opt/osa-warp/RE_ENV; \
              pushd /opt/osa-warp; \
              export TERM=linux
              export SKIP_PREFLIGHT=true
              ./osa-warp.sh ${RE_JOB_UPGRADE_TO}"
echo "OSA Warp completed..."
